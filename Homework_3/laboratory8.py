import math
def fun(x, e):
    y = math.sin(x) + pow(e, x) + 1
    return round(y,3)

for x in range(0, 3):
    for e in range(0, 3):
        open('myFiles/function1.txt', 'a').write(str(fun(x,e))+', ')
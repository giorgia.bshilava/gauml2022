def gamyofebis_raodenoba(i):
    arr = []
    for t in range(1, i + 1):
        if i % t == 0:
            arr.append(t)
    if len(arr) != 4:
        return False
    else:
        return True

for i in range(10, 500):
    if i % 7 == 0 and gamyofebis_raodenoba(i):
        print(i)



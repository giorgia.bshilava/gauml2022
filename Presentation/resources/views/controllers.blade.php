<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Controllers</title>
</head>

<body>
    <?php
	
		 
			$arr = [10,15,20,25,20,30,30,40];
			// echo collect($arr)->average();

			// echo collect($arr)->max();

			// echo collect($arr)->min();
			
			// echo collect($arr)->median();

			$arr = [
				[1,2,3],
				[4,5,6],
				];

			// echo collect($arr)->collapse();
			$arr = [1,2,3,4,5,6,7,8];
			// print_r(collect($arr)->chunk(4));


			$keys = collect(["key1", "key2"]);
			$values = collect(["value1", "value2"]);

			// echo $keys->combine($values);

			$arr = collect(["Barbare"]);

			// echo $arr->concat(["Jajanidze"]);
			 
			$arr = collect(["Barbare Jajanidze", "Vitali Gergedava", "777"]);

			// echo $arr->contains(777); //strict

			// echo $arr->count();

			$arr = collect([1,2]);

			// echo $arr->crossJoin(["a","b"]);

			$arr = collect([1,2,3]);

			//  print_r($arr->diff([2,4,6]));

			$arr = collect([1,2,3]);

			// $arr->tap(function($arr2){
			// 	$arr2->each(function($value){
			// 		dump("tap ". $value);
			// 	});
			// });

			
			// $arr->map(function($element){
			// 	 echo $element * 2;
			// });

			$dict = collect([
				["name" => "Banana", "Price" => 1.00],
				["name" => "Apple", "Price" => 1.10],
				["name" => "Watermelon", "Price" => 2.20],
				["name" => "Pineapple", "Price" => 1.30],
			]);

			// echo $dict->where("Price", "<",  2.20); //first

			// echo $dict->whereBetween("Price", [1.10 , 1.30]);

			// echo $dict->whereNotBetween("Price", [1.10 , 1.30]);

			// echo $dict->whereIn("Price", [1.10 , 2.20]); //strict

			// echo $dict->whereNotIn("Price", [1.10 , 2.20]); //strict

			// echo collect([1,2,3, null, "", []])->filter();

			// echo $dict->pluck("name");

			// echo collect([21,33,12,15,56])->sort();
			
			// echo $dict->sortBy("Price");

			$dict = collect([
				["name" => "Banana", "Price" => 1.50],
				["name" => "Apple", "Price" => 1.90],
				["name" => "Watermelon", "Price" => 2.20],
				["name" => "Banana", "Price" => 1.99],
				["name" => "Apple", "Price" => 1.10],
				["name" => "Banana", "Price" => 1.75],
				["name" => "Apple", "Price" => 1.15],
				["name" => "Watermelon", "Price" => 2.20],
				["name" => "Pineapple", "Price" => 1.30],
			]);

			//  echo $dict->groupBy("name");



			





		 
		

	

	
	
    ?>

</body>

</html>

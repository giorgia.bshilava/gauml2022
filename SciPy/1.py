# from scipy import linalg
# help(integrate.odeint)

# from scipy import linalg
# from scipy import source
# source(linalg.inv)


# from scipy import special
# print(special.sindg(90))


# from scipy import integrate
# import numpy as np
#
# def target_function(y, x):
#     return -2.0 * y
#
# xi = np.linspace(0, 1, 10)
# y0 = 1.0
# result = integrate.odeint(target_function, y0, xi)
# print(result)



# import numpy as np
# import matplotlib.pyplot as plt
# from scipy import interpolate
#
# x = np.arange(5, 25)
# y = np.exp(x/3.0)
#
# f = interpolate.interp1d(x, y)
#
# x1 = np.arange(6, 12)
# y1 = f(x1)
#
# plt.plot(x, y, 'o', x1, y1, '--')
# plt.show()



# import numpy as np
# from scipy.optimize import root
#
# def target_function(x):
#    return x ** 2 + 2 * np.cos(x)
#
# x0 = 0.3
# result = root(target_function, x0)
# print(result)

# from scipy import constants
#
# print(constants.pi)
# print(constants.c)
# print(constants.G)
# print(constants.g)
# print(constants.m_e)
# print(constants.m_p)


# from scipy import linalg
# import numpy as np
#
#
# a = np.array([[1, 3, 5],
#               [2, 5, 1],
#               [2, 3, 8]])
#
#
# b = np.array([10,
#               8,
#               3])
#
# result = linalg.solve(a, b)
# print(result)


# from scipy import linalg
# import numpy as np
#
#
# A = np.array([[1,2],
#               [3,4]])
#
# deter = linalg.det(A)
# print(deter)


# from scipy import linalg
# import numpy as np
# A = np.array([[1,2],
#               [4,3]])
# print('A:')
# print(A)
#
# B = linalg.inv(A)
#
# print('\ninversed A:')
# print(B)


# from scipy import misc
# f = misc.face()
#
# import matplotlib.pyplot as plt
# plt.imshow(f)
# plt.show()

# import numpy as np
# from scipy import stats
# x = np.arange(20)
# print(x)
# print(stats.tstd(x))
#



first = [1,2,3,4,5]
second = [3,2,5,3,5,2,4]
third = [1,1,1,1,1,1,1]

def multiply(array):
    result = 1
    for i in range(len(array)):
        result *= array[i]
    return result


print(multiply(first))
print(multiply(second))
print(multiply(third))

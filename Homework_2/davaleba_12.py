import math
def factorial(n):
    return math.factorial(n)

print(factorial(5))
print(factorial(4))
print(factorial(3))
print(factorial(2))
print(factorial(1))
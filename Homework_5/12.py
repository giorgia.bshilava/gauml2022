import numpy

print(numpy.linalg.det([[2,4],[5,3]]))
print(numpy.linalg.det([[[2,4],[5,3]],[[2,4],[2,1]]]))
print(numpy.linalg.det([[[[ 2,  3],
         [ 2,  2]],

        [[ 5,  6],
         [ 3,  6]]],


       [[[ 3, 2],
         [ 2,  1]],

        [[ 4,  4],
         [ 2,  3]]]]))

 